# Documentation Bootstrap

## Table of Contents
  - [Requirements](#requirements)
  - [Development server](#development-server)
  - [How to contribute](#how-to-contribute)

## Requirements
* Node.js version 10+
* Yarn

## Development server
```sh
yarn install
yarn dev
```
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files except for the ones relating to the configuration for which `yarn dev` will have to be re run.

## How to contribute
Open an MR and contact one of the contributors

## Tips

- Configure the home page in the src/index.md file
- Configure the side navs and nav bar in the src/.vuepress/config.js file