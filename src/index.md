---
home: true
heroImage: /welcome-image.png
tagline: Android course
actionText: Get started →
actionLink: /android/getting-started/guidelines/general/
features:
- title: Find a subject 💡
  details: Choose an API from all the free ones available online and create a simple use case around the API theme.
- title: Build the app 🖥️
  details: Build the app using most popular and modern tools.
- title: Deploy the app 🚀
  details: Deploy your app to Firebase.
footer: Made by Maxime MICHEL and Simon MERCIER from Worldline. Contact us at tpinsa.droid@gmail.com.
---

